﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using RemoteDesktopClient;

namespace RemoteDesktopClient
{
    public partial class Form1 : Form
    {
        bool ViewOnlyMode = false;
        HashAlgorithm sha = SHA512.Create();
        ChannelFactory<IRemote> channelFactory = null;
        EndpointAddress ep = null;
        IRemote Srv = null;
        Size HostScreenSize;
        int TimerMenuTime = 0;
        bool NeedCenteringCursor = false;
        Stopwatch FPSCounter = new Stopwatch();

        public Form1()
        {
            InitializeComponent();
            this.MouseWheel += new MouseEventHandler(this.pictureBox1_MouseWheel);
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            if (timerDraw.Enabled)
                Disconnect();
            else
                Connect();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Disconnect();
        }

        private void Connect()
        {
            string strEPAdr = "net.tcp://" + textBox1.Text + ":" + numericUpDown1.Value.ToString() + "/ScreenControl/";
            try
            {
                ep = new EndpointAddress(strEPAdr);
                NetTcpBinding tcpb = new NetTcpBinding(SecurityMode.None);

                tcpb.MaxReceivedMessageSize = 1024 * 1024 * 8;
                tcpb.MaxBufferSize = 1024 * 1024 * 8;

                XmlDictionaryReaderQuotas myReaderQuotas = new XmlDictionaryReaderQuotas();
                myReaderQuotas.MaxStringContentLength = 1024 * 1024 * 8;
                myReaderQuotas.MaxArrayLength = 1024 * 1024 * 8;
                myReaderQuotas.MaxBytesPerRead = 1024 * 1024 * 8;
                myReaderQuotas.MaxDepth = 1024 * 1024 * 8;
                myReaderQuotas.MaxNameTableCharCount = 1024 * 1024 * 8;
                tcpb.GetType().GetProperty("ReaderQuotas").SetValue(tcpb, myReaderQuotas, null);

                channelFactory = new ChannelFactory<IRemote>(tcpb);
                Srv = channelFactory.CreateChannel(ep);
                Text = "Connecting to Host...";

                if (Srv.CheckPassword(PasswordHash(textBoxPass.Text, Srv.GetPasswordData())))
                {
                    ViewOnlyMode = Srv.IsViewOnlyMode();
                    HostScreenSize = Srv.GetScreenResolution();
                    timerDraw.Enabled = true;
                    Cursor = new Cursor(Cursor.Current.Handle);
                    Text = "Connected.";
                }
                else
                {
                    Disconnect();
                    MessageBox.Show("Wrong password.");
                }
            }
            catch (Exception eX)
            {
                Error(eX);
            }
        }

        private void Disconnect()
        {
            if (timerDraw.Enabled)
            {
                timerDraw.Enabled = false;
                Text = "Disconnected.";
            }
            try
            {
                channelFactory.Close();
            }
            catch { }
        }

        private byte[] PasswordHash(string Password, byte[] data)
        {
            byte[] key = sha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(Password));
            for (int i = 0; i < data.Length; i++)
            {
                if (i % 2 == 0)
                    data[i] = (byte)~data[i];
                data[i] ^= key[i];
                data[i] ^= key[data.Length - i - 1];
                if (i % 2 == 0)
                    data[i] = (byte)~data[i];
            }
            return sha.ComputeHash(data);
        }

        private void timerDraw_Tick(object sender, EventArgs e)
        {
            FPSCounter.Start();
            try
            {
                pictureBox1.Image = byteArrayToBitmap(Srv.GetScreenShot((long)numericUpDownQuality.Value));
            }
            catch (Exception eX)
            {
                Error(eX);
            }
            FPSCounter.Stop();
            if (checkBoxShowFPS.Checked)
                Text = "FPS = " + ((int)(1000.0f / (FPSCounter.ElapsedMilliseconds + 0.001f))).ToString();
            FPSCounter.Reset();
        }

        public Image byteArrayToBitmap(byte[] byteArray)
        {
            Image image;
            using (MemoryStream ms = new MemoryStream(byteArray))
                image = Image.FromStream(ms);
            return image;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (timerDraw.Enabled && !ViewOnlyMode)
                try
                {
                    if (NeedCenteringCursor)
                    {
                        NeedCenteringCursor = false;
                        return;
                    }

                    Srv.SetCursorPosition(e.X * HostScreenSize.Width / pictureBox1.Width, e.Y * HostScreenSize.Height / pictureBox1.Height);

                    if (checkBoxCenterCursor.Checked)
                    {
                        Cursor.Position = new Point(Left + pictureBox1.Left + pictureBox1.Width / 2, Top + pictureBox1.Top + pictureBox1.Height / 2);
                        NeedCenteringCursor = true;
                    }
                }
                catch (Exception eX)
                {
                    Error(eX);
                }

            if (e.X <= buttonMenuShow.Width && e.Y <= buttonMenuShow.Height && !groupBoxMenu.Visible)
                timerMenu.Enabled = true;
            else
                buttonMenuShow.Visible = false;
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (timerDraw.Enabled && !ViewOnlyMode)
                try
                {
                    Srv.MouseUp(e.Button);
                }
                catch (Exception eX)
                {
                    Error(eX);
                }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (timerDraw.Enabled && !ViewOnlyMode)
                try
                {
                    Srv.MouseDown(e.Button);
                }
                catch (Exception eX)
                {
                    Error(eX);
                }
        }

        private void pictureBox1_MouseWheel(object sender, MouseEventArgs e)
        {
            if (timerDraw.Enabled && !ViewOnlyMode)
                try
                {
                    Srv.MouseWheel(e.Delta);
                }
                catch (Exception eX)
                {
                    Error(eX);
                }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.Alt && e.KeyCode == Keys.F)
            {
                checkBoxCenterCursor.Checked = !checkBoxCenterCursor.Checked;
                return;
            }

            if (timerDraw.Enabled && !ViewOnlyMode)
                try
                {
                    Srv.KeyPress((byte)e.KeyValue, 0);
                }
                catch (Exception eX)
                {
                    Error(eX);
                }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (timerDraw.Enabled && !ViewOnlyMode)
                try
                {
                    Srv.KeyPress((byte)e.KeyValue, 2);
                }
                catch (Exception eX)
                {
                    Error(eX);
                }
        }

        private void Error(Exception eX)
        {
            timerDraw.Enabled = false;
            MessageBox.Show("Error while performing operation [" + eX.Message + "] \n\n Inner Exception [" + eX.InnerException + "]");
        }

        private void buttonMenuShow_Click(object sender, EventArgs e)
        {
            groupBoxMenu.Visible = true;
            buttonMenuShow.Visible = false;
            timerMenu.Enabled = false;
        }

        private void buttonMenuHide_Click(object sender, EventArgs e)
        {
            groupBoxMenu.Visible = false;
            pictureBox1.Focus();
        }

        private void timerMenu_Tick(object sender, EventArgs e)
        {
            TimerMenuTime++;
            if (TimerMenuTime >= 15)
            {
                buttonMenuShow.Visible = true;
                timerMenu.Enabled = false;
                TimerMenuTime = 0;
            }
        }

        private void buttonAcrossTheWidth_Click(object sender, EventArgs e)
        {
            if (timerDraw.Enabled)
            {
                try
                {
                    HostScreenSize = Srv.GetScreenResolution();
                    this.Width = this.Height * HostScreenSize.Width / HostScreenSize.Height + (this.Width - pictureBox1.Width);
                    if (this.Width > Screen.PrimaryScreen.WorkingArea.Width)
                    {
                        this.Width = Screen.PrimaryScreen.WorkingArea.Width;
                        this.Height = this.Width * HostScreenSize.Height / HostScreenSize.Width + (this.Height - pictureBox1.Height);
                    }
                    FormToCenter();
                }
                catch { }
            }
        }

        private void buttonAcrossTheHeight_Click(object sender, EventArgs e)
        {
            if (timerDraw.Enabled)
            {
                try
                {
                    HostScreenSize = Srv.GetScreenResolution();
                    this.Height = this.Width * HostScreenSize.Height / HostScreenSize.Width + (this.Height - pictureBox1.Height);
                    if (this.Height > Screen.PrimaryScreen.WorkingArea.Height)
                    {
                        this.Height = Screen.PrimaryScreen.WorkingArea.Height;
                        this.Width = this.Height * HostScreenSize.Width / HostScreenSize.Height + (this.Width - pictureBox1.Width);
                    }
                    FormToCenter();
                }
                catch { }
            }
        }

        private void buttonToScreenSize_Click(object sender, EventArgs e)
        {
            if (timerDraw.Enabled)
            {
                try
                {
                    this.Width = Screen.PrimaryScreen.WorkingArea.Width;
                    this.Height = Screen.PrimaryScreen.WorkingArea.Height;
                    buttonAcrossTheWidth_Click(sender, e);
                    buttonAcrossTheHeight_Click(sender, e);
                }
                catch { }
            }
        }

        private void FormToCenter()
        {
            this.Left = (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2;
            this.Top = (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2;
        }
    }
}
