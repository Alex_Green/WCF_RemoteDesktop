﻿namespace RemoteDesktopClient
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBoxMenu = new System.Windows.Forms.GroupBox();
            this.buttonToScreenSize = new System.Windows.Forms.Button();
            this.buttonAcrossTheHeight = new System.Windows.Forms.Button();
            this.buttonAcrossTheWidth = new System.Windows.Forms.Button();
            this.checkBoxShowFPS = new System.Windows.Forms.CheckBox();
            this.labelJPGQuality = new System.Windows.Forms.Label();
            this.labelPort = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.numericUpDownQuality = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.textBoxPass = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonMenuHide = new System.Windows.Forms.Button();
            this.timerMenu = new System.Windows.Forms.Timer(this.components);
            this.buttonMenuShow = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timerDraw = new System.Windows.Forms.Timer(this.components);
            this.checkBoxCenterCursor = new System.Windows.Forms.CheckBox();
            this.groupBoxMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQuality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxMenu
            // 
            this.groupBoxMenu.Controls.Add(this.buttonToScreenSize);
            this.groupBoxMenu.Controls.Add(this.buttonAcrossTheHeight);
            this.groupBoxMenu.Controls.Add(this.buttonAcrossTheWidth);
            this.groupBoxMenu.Controls.Add(this.labelJPGQuality);
            this.groupBoxMenu.Controls.Add(this.labelPort);
            this.groupBoxMenu.Controls.Add(this.labelAddress);
            this.groupBoxMenu.Controls.Add(this.labelPassword);
            this.groupBoxMenu.Controls.Add(this.numericUpDownQuality);
            this.groupBoxMenu.Controls.Add(this.numericUpDown1);
            this.groupBoxMenu.Controls.Add(this.buttonConnect);
            this.groupBoxMenu.Controls.Add(this.textBoxPass);
            this.groupBoxMenu.Controls.Add(this.textBox1);
            this.groupBoxMenu.Controls.Add(this.buttonMenuHide);
            this.groupBoxMenu.Controls.Add(this.checkBoxCenterCursor);
            this.groupBoxMenu.Controls.Add(this.checkBoxShowFPS);
            this.groupBoxMenu.Location = new System.Drawing.Point(0, 0);
            this.groupBoxMenu.Name = "groupBoxMenu";
            this.groupBoxMenu.Size = new System.Drawing.Size(380, 108);
            this.groupBoxMenu.TabIndex = 5;
            this.groupBoxMenu.TabStop = false;
            // 
            // buttonToScreenSize
            // 
            this.buttonToScreenSize.Image = ((System.Drawing.Image)(resources.GetObject("buttonToScreenSize.Image")));
            this.buttonToScreenSize.Location = new System.Drawing.Point(203, 70);
            this.buttonToScreenSize.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.buttonToScreenSize.Name = "buttonToScreenSize";
            this.buttonToScreenSize.Size = new System.Drawing.Size(32, 32);
            this.buttonToScreenSize.TabIndex = 27;
            this.buttonToScreenSize.UseVisualStyleBackColor = true;
            this.buttonToScreenSize.Click += new System.EventHandler(this.buttonToScreenSize_Click);
            // 
            // buttonAcrossTheHeight
            // 
            this.buttonAcrossTheHeight.Image = ((System.Drawing.Image)(resources.GetObject("buttonAcrossTheHeight.Image")));
            this.buttonAcrossTheHeight.Location = new System.Drawing.Point(169, 70);
            this.buttonAcrossTheHeight.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.buttonAcrossTheHeight.Name = "buttonAcrossTheHeight";
            this.buttonAcrossTheHeight.Size = new System.Drawing.Size(32, 32);
            this.buttonAcrossTheHeight.TabIndex = 26;
            this.buttonAcrossTheHeight.UseVisualStyleBackColor = true;
            this.buttonAcrossTheHeight.Click += new System.EventHandler(this.buttonAcrossTheHeight_Click);
            // 
            // buttonAcrossTheWidth
            // 
            this.buttonAcrossTheWidth.Image = ((System.Drawing.Image)(resources.GetObject("buttonAcrossTheWidth.Image")));
            this.buttonAcrossTheWidth.Location = new System.Drawing.Point(135, 70);
            this.buttonAcrossTheWidth.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.buttonAcrossTheWidth.Name = "buttonAcrossTheWidth";
            this.buttonAcrossTheWidth.Size = new System.Drawing.Size(32, 32);
            this.buttonAcrossTheWidth.TabIndex = 25;
            this.buttonAcrossTheWidth.UseVisualStyleBackColor = true;
            this.buttonAcrossTheWidth.Click += new System.EventHandler(this.buttonAcrossTheWidth_Click);
            // 
            // checkBoxShowFPS
            // 
            this.checkBoxShowFPS.Location = new System.Drawing.Point(279, 39);
            this.checkBoxShowFPS.Name = "checkBoxShowFPS";
            this.checkBoxShowFPS.Size = new System.Drawing.Size(94, 21);
            this.checkBoxShowFPS.TabIndex = 9;
            this.checkBoxShowFPS.Text = "Show FPS";
            this.checkBoxShowFPS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxShowFPS.UseVisualStyleBackColor = true;
            // 
            // labelJPGQuality
            // 
            this.labelJPGQuality.Location = new System.Drawing.Point(247, 76);
            this.labelJPGQuality.Name = "labelJPGQuality";
            this.labelJPGQuality.Size = new System.Drawing.Size(66, 20);
            this.labelJPGQuality.TabIndex = 24;
            this.labelJPGQuality.Text = "JPG Quality:";
            this.labelJPGQuality.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPort
            // 
            this.labelPort.Location = new System.Drawing.Point(276, 18);
            this.labelPort.Name = "labelPort";
            this.labelPort.Size = new System.Drawing.Size(32, 20);
            this.labelPort.TabIndex = 23;
            this.labelPort.Text = "Port:";
            this.labelPort.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAddress
            // 
            this.labelAddress.Location = new System.Drawing.Point(60, 17);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(69, 20);
            this.labelAddress.TabIndex = 22;
            this.labelAddress.Text = "Address:";
            this.labelAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPassword
            // 
            this.labelPassword.Location = new System.Drawing.Point(63, 44);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(66, 20);
            this.labelPassword.TabIndex = 21;
            this.labelPassword.Text = "Password:";
            this.labelPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numericUpDownQuality
            // 
            this.numericUpDownQuality.Location = new System.Drawing.Point(319, 76);
            this.numericUpDownQuality.Name = "numericUpDownQuality";
            this.numericUpDownQuality.Size = new System.Drawing.Size(54, 20);
            this.numericUpDownQuality.TabIndex = 18;
            this.numericUpDownQuality.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownQuality.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(314, 18);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(59, 20);
            this.numericUpDown1.TabIndex = 17;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown1.Value = new decimal(new int[] {
            8000,
            0,
            0,
            0});
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(6, 70);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(123, 32);
            this.buttonConnect.TabIndex = 15;
            this.buttonConnect.Text = "Connect / Disconnect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // textBoxPass
            // 
            this.textBoxPass.Location = new System.Drawing.Point(135, 44);
            this.textBoxPass.Name = "textBoxPass";
            this.textBoxPass.Size = new System.Drawing.Size(135, 20);
            this.textBoxPass.TabIndex = 20;
            this.textBoxPass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxPass.UseSystemPasswordChar = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(135, 17);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(135, 20);
            this.textBox1.TabIndex = 16;
            this.textBox1.Text = "localhost";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonMenuHide
            // 
            this.buttonMenuHide.Image = ((System.Drawing.Image)(resources.GetObject("buttonMenuHide.Image")));
            this.buttonMenuHide.Location = new System.Drawing.Point(6, 12);
            this.buttonMenuHide.Name = "buttonMenuHide";
            this.buttonMenuHide.Size = new System.Drawing.Size(48, 48);
            this.buttonMenuHide.TabIndex = 8;
            this.buttonMenuHide.UseVisualStyleBackColor = true;
            this.buttonMenuHide.Click += new System.EventHandler(this.buttonMenuHide_Click);
            // 
            // timerMenu
            // 
            this.timerMenu.Tick += new System.EventHandler(this.timerMenu_Tick);
            // 
            // buttonMenuShow
            // 
            this.buttonMenuShow.Image = ((System.Drawing.Image)(resources.GetObject("buttonMenuShow.Image")));
            this.buttonMenuShow.Location = new System.Drawing.Point(0, 0);
            this.buttonMenuShow.Name = "buttonMenuShow";
            this.buttonMenuShow.Size = new System.Drawing.Size(48, 48);
            this.buttonMenuShow.TabIndex = 8;
            this.buttonMenuShow.UseVisualStyleBackColor = true;
            this.buttonMenuShow.Visible = false;
            this.buttonMenuShow.Click += new System.EventHandler(this.buttonMenuShow_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(624, 441);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // timerDraw
            // 
            this.timerDraw.Interval = 1;
            this.timerDraw.Tick += new System.EventHandler(this.timerDraw_Tick);
            // 
            // checkBoxCenterCursor
            // 
            this.checkBoxCenterCursor.Location = new System.Drawing.Point(279, 60);
            this.checkBoxCenterCursor.Name = "checkBoxCenterCursor";
            this.checkBoxCenterCursor.Size = new System.Drawing.Size(94, 19);
            this.checkBoxCenterCursor.TabIndex = 28;
            this.checkBoxCenterCursor.Text = "Center Cursor";
            this.checkBoxCenterCursor.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 441);
            this.Controls.Add(this.groupBoxMenu);
            this.Controls.Add(this.buttonMenuShow);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Remote Desktop Client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.groupBoxMenu.ResumeLayout(false);
            this.groupBoxMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQuality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxMenu;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonMenuHide;
        private System.Windows.Forms.Button buttonMenuShow;
        private System.Windows.Forms.Timer timerMenu;
        private System.Windows.Forms.Label labelJPGQuality;
        private System.Windows.Forms.Label labelPort;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.NumericUpDown numericUpDownQuality;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.TextBox textBoxPass;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.CheckBox checkBoxShowFPS;
        private System.Windows.Forms.Button buttonAcrossTheWidth;
        private System.Windows.Forms.Button buttonAcrossTheHeight;
        private System.Windows.Forms.Button buttonToScreenSize;
        private System.Windows.Forms.Timer timerDraw;
        private System.Windows.Forms.CheckBox checkBoxCenterCursor;
    }
}

