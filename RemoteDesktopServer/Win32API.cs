﻿using System;
using System.Runtime.InteropServices;

namespace Win32API
{
    [Flags]
    public enum MouseEventFlags : uint
    {
        Move = 0x00000001,
        LeftDown = 0x00000002,
        LeftUp = 0x00000004,
        RightDown = 0x00000008,
        RightUp = 0x00000010,
        MiddleDown = 0x00000020,
        MiddleUp = 0x00000040,
        XDown = 0x00000080,
        XUp = 0x00000100,
        Wheel = 0x00000800,
        HWheel = 0x00001000,
        Absolute = 0x00008000
    }

    public static class API
    {
        [DllImport("user32.dll")]
        public static extern bool SetCursorPos(int X, int Y);

        [DllImport("user32.dll")]
        public static extern void mouse_event(MouseEventFlags dwFlags, uint dx, uint dy, int dwData, UIntPtr dwExtraInfo);

        [DllImport("user32.dll")]
        public static extern bool keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);
    }
}