﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Security.Cryptography;
using System.Windows.Forms;
using Win32API;

namespace RemoteDesktopServer
{
    public class Remote : IRemote
    {
        #region Все, что не касается реализации IRemote (экземпляры классов, дополнительные функции)
        static Random R = new Random();
        static HashAlgorithm sha = SHA512.Create();
        byte[] PassRand = new byte[sha.HashSize];
        bool IsPasswordCorrect = false;

        ImageCodecInfo jgpEncoder;
        Encoder myEncoder = Encoder.Quality;
        EncoderParameters myEncoderParameters = new EncoderParameters(1);
        EncoderParameter myEncoderParameter;

        public Remote()
        {
            R.NextBytes(PassRand);
            jgpEncoder = GetEncoder(ImageFormat.Jpeg);
        }

        private byte[] ImageToByte(Bitmap bmp, long Quality)
        {
            byte[] byteArray;
            using (MemoryStream stream = new MemoryStream())
            {
                myEncoderParameter = new EncoderParameter(myEncoder, Quality);
                myEncoderParameters.Param[0] = myEncoderParameter;
                bmp.Save(stream, jgpEncoder, myEncoderParameters);
                stream.Close();
                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
                if (codec.FormatID == format.Guid)
                    return codec;
            return null;
        }
        #endregion

        public byte[] GetPasswordData()
        {
            R.NextBytes(PassRand);
            byte[] key = sha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(Program.Password));
            byte[] data = new byte[key.Length];

            for (int i = 0; i < data.Length; i++)
            {
                data[i] = (byte)(PassRand[i] ^ key[i]);
                if (i % 2 == 0)
                    data[i] = (byte)~data[i];
            }
            return data;
        }

        public bool CheckPassword(byte[] data)
        {
            byte[] key = sha.ComputeHash(System.Text.Encoding.UTF8.GetBytes(Program.Password));
            byte[] code = new byte[key.Length];

            for (int i = 0; i < code.Length; i++)
            {
                code[i] = (byte)(PassRand[i] ^ key[code.Length - i - 1]);
                if (i % 2 == 0)
                    code[i] = (byte)~code[i];
            }
            code = sha.ComputeHash(code);

            int ValidBytes = 0;
            for (int i = 0; i < code.Length; i++)
                if (data[i] == code[i])
                    ValidBytes++;
            if (ValidBytes == code.Length)
            {
                IsPasswordCorrect = true;
                Console.WriteLine("Client Connected. Password is correct.");
                return true;
            }
            else
            {
                IsPasswordCorrect = false;
                GetPasswordData();
                Console.WriteLine("Connection attempt failed. Password is incorrect!");
                return false;
            }
        }

        public Size GetScreenResolution()
        {
            if (IsPasswordCorrect)
                return Screen.PrimaryScreen.Bounds.Size;
            else
                return new Size(0, 0);
        }

        public byte[] GetScreenShot(long Quality)
        {
            if (IsPasswordCorrect)
            {
                Bitmap bmp = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
                using (Graphics G = Graphics.FromImage(bmp))
                    G.CopyFromScreen(0, 0, 0, 0, bmp.Size);
                return ImageToByte(bmp, Quality);
            }
            else
                return null;
        }

        public bool IsViewOnlyMode()
        {
            if (IsPasswordCorrect)
                return Program.ViewOnlyMode;
            else
                return true;
        }

        public void SetCursorPosition(int X, int Y)
        {
            if (IsPasswordCorrect && !Program.ViewOnlyMode)
                API.SetCursorPos(X, Y);
        }

        public void MouseUp(MouseButtons MouseButton)
        {
            if (IsPasswordCorrect && !Program.ViewOnlyMode)
                switch (MouseButton)
                {
                    case MouseButtons.Left:
                        API.mouse_event(MouseEventFlags.LeftUp, 0, 0, 0, UIntPtr.Zero);
                        break;
                    case MouseButtons.Middle:
                        API.mouse_event(MouseEventFlags.MiddleUp, 0, 0, 0, UIntPtr.Zero);
                        break;
                    case MouseButtons.Right:
                        API.mouse_event(MouseEventFlags.RightUp, 0, 0, 0, UIntPtr.Zero);
                        break;
                    case MouseButtons.XButton1:
                        API.mouse_event(MouseEventFlags.XUp, 0, 0, 1, UIntPtr.Zero);
                        break;
                    case MouseButtons.XButton2:
                        API.mouse_event(MouseEventFlags.XUp, 0, 0, 2, UIntPtr.Zero);
                        break;
                }
            Console.WriteLine("Mouse {0} Up", MouseButton.ToString());
        }

        public void MouseDown(MouseButtons MouseButton)
        {
            if (IsPasswordCorrect && !Program.ViewOnlyMode)
                switch (MouseButton)
                {
                    case MouseButtons.Left:
                        API.mouse_event(MouseEventFlags.LeftDown, 0, 0, 0, UIntPtr.Zero);
                        break;
                    case MouseButtons.Middle:
                        API.mouse_event(MouseEventFlags.MiddleDown, 0, 0, 0, UIntPtr.Zero);
                        break;
                    case MouseButtons.Right:
                        API.mouse_event(MouseEventFlags.RightDown, 0, 0, 0, UIntPtr.Zero);
                        break;
                    case MouseButtons.XButton1:
                        API.mouse_event(MouseEventFlags.XDown, 0, 0, 1, UIntPtr.Zero);
                        break;
                    case MouseButtons.XButton2:
                        API.mouse_event(MouseEventFlags.XDown, 0, 0, 2, UIntPtr.Zero);
                        break;
                }
            Console.WriteLine("Mouse {0} Down", MouseButton.ToString());
        }

        public void MouseWheel(int Delta)
        {
            if (IsPasswordCorrect && !Program.ViewOnlyMode)
                API.mouse_event(MouseEventFlags.Wheel, 0, 0, Delta, UIntPtr.Zero);
            Console.WriteLine("Mouse Scroll, Delta = {0}", Delta);
        }

        public void KeyPress(byte bVk, uint dwFlags)
        {
            if (IsPasswordCorrect && !Program.ViewOnlyMode)
                API.keybd_event(bVk, 0, dwFlags, UIntPtr.Zero);
            Console.WriteLine("Key = {0}, \tFlags = {1}", bVk, dwFlags);
        }
    }
}
