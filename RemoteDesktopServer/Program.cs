﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace RemoteDesktopServer
{
    class Program
    {
        static ServiceHost svcHost = null;
        public static string Password = String.Empty;
        public static bool ViewOnlyMode = false;

        static void Main(string[] args)
        {
            int Port = 8000;
            string IP = "localhost";

            #region Start Commands
            foreach (string c in args)
            {
                try
                {
                    string cmd = c.Substring(1, c.Length - 1); //Преобразуем "-Command=Value" в "Command=Value"
                    string[] commands = new string[] { "Help", "Address", "Port", "Password", "ViewOnly" };

                    for (int i = 0; i < commands.Length; i++)
                    {
                        if (cmd.Substring(0, commands[i].Length).ToUpper() == commands[i].ToUpper())
                        {
                            switch (i)
                            {
                                case 0:
                                    Console.WriteLine("Aviable commands: Help, Address, Port, Password, ViewOnly.");
                                    Environment.Exit(0);
                                    break;
                                case 1:
                                    IP = cmd.Substring(commands[i].Length + 1, cmd.Length - commands[i].Length - 1);
                                    break;
                                case 2:
                                    Port = Convert.ToInt32(cmd.Substring(commands[i].Length + 1, cmd.Length - commands[i].Length - 1));
                                    break;
                                case 3:
                                    Password = cmd.Substring(commands[i].Length + 1, cmd.Length - commands[i].Length - 1);
                                    break;
                                case 4:
                                    ViewOnlyMode = Convert.ToBoolean(cmd.Substring(commands[i].Length + 1, cmd.Length - commands[i].Length - 1));
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                catch { }
            }
            #endregion

            StartTCPService(IP, Port);
            Console.WriteLine("Press any key to close...");
            Console.ReadKey();
            StopService();
        }

        private static void StartTCPService(string IP, int Port)
        {
            string strAdr = "net.tcp://" + IP + ":" + Port.ToString() + "/ScreenControl/";
            try
            {
                svcHost = new ServiceHost(typeof(Remote), new Uri(strAdr));
                svcHost.AddServiceEndpoint(typeof(IRemote), new NetTcpBinding(SecurityMode.None), strAdr);
                svcHost.Open();
                Console.WriteLine("Service is Running as >> " + strAdr);
            }
            catch (Exception eX)
            {
                svcHost = null;
                Console.WriteLine("Service can not be started as >> [" + strAdr + "] \n\nError Message [" + eX.Message + "]");
            }
        }

        private static void StopService()
        {
            if (svcHost != null)
            {
                svcHost.Close();
                svcHost = null;
            }
        }
    }
}
