﻿using System;
using System.Drawing;
using System.ServiceModel;
using System.Windows.Forms;

namespace RemoteDesktopServer
{
    [ServiceContract]
    public interface IRemote
    {
        [OperationContract]
        byte[] GetPasswordData();

        [OperationContract]
        bool CheckPassword(byte[] data);

        [OperationContract]
        Size GetScreenResolution();

        [OperationContract]
        byte[] GetScreenShot(long Quality);

        [OperationContract]
        bool IsViewOnlyMode();

        [OperationContract]
        void SetCursorPosition(int X, int Y);

        [OperationContract]
        void MouseUp(MouseButtons MouseButton);

        [OperationContract]
        void MouseDown(MouseButtons MouseButton);

        [OperationContract]
        void MouseWheel(int Delta);

        [OperationContract]
        void KeyPress(byte bVk, uint dwFlags);
    }
}
